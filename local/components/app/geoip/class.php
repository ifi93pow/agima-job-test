<?php

/**
 * @property \Bitrix\Main\Context $context
 * @property \Bitrix\Main\Server $server
 */
class CAppGeoip extends CBitrixComponent
{
    public function __construct($component)
    {
        parent::__construct($component);

        $this->context = \Bitrix\Main\Application::getInstance()->getContext();
        $this->server = $this->context->getServer();
    }

    public function onPrepareComponentParams($arParams)
    {
        $arParams['IS_AJAX'] = $this->request->isAjaxRequest();
        $arParams['IP'] = $this->request->get('ip') ? $this->request->get('ip') : $this->server->get('REMOTE_ADDR');

        return $arParams;
    }

    public function executeComponent()
    {
        global $APPLICATION;

        if ($this->arParams['IS_AJAX']) {
            $APPLICATION->RestartBuffer();
            $this->setTemplateName('ajax');
            header('Content-Type: application/json');
        }

        if ($this->request->isPost() && $this->request->get('form') === 'geoip') {
            try {
                if (!class_exists('SoapClient')) throw new Exception('Class SoapClient not exists');
                if (!check_bitrix_sessid()) throw new Exception('Некорректная сессия, попробуйте заполнить форму заново.');
                if (!$this->arParams['IP']) throw new Exception('Не указан IP');
                if (!filter_var($this->arParams['IP'], FILTER_VALIDATE_IP)) throw new Exception('Указанный IP некорректен');

                $this->arResult['RESULT'] = $this->getDataByIp($this->arParams['IP']);
                $this->arResult['RESULT_STR'] = json_encode($this->arResult['RESULT'], JSON_PRETTY_PRINT && JSON_UNESCAPED_UNICODE && JSON_UNESCAPED_SLASHES);
                $this->arResult['RESULT_STR'] = str_replace(',"', ",\n\"", $this->arResult['RESULT_STR']);
                $this->arResult['RESULT_STR'] = trim($this->arResult['RESULT_STR'], "{} \t\n");

            } catch (Exception $e) {
                $this->arResult['ERROR'] = $e->getMessage();
                $this->addError2Log($this->arResult['ERROR'], print_r($_REQUEST, true));
                http_response_code(400);
            }
        }
        $this->includeComponentTemplate();

        if ($this->arParams['IS_AJAX']) {
            die;
        }
    }

    public function getDataByIp($ip)
    {
        $result = $this->getDataByIpCached($ip);

        if (!is_object($result) || !isset($result->ReturnCode)) {
            throw new Exception('Некорректный ответ сервиса');
        }
        if ($result->ReturnCode !== 1) {
            throw new Exception('Ошибка при обработке запроса: ' . $result->ReturnCodeDetails);
        }

        return $result;
    }

    public function getDataByIpRaw($ip)
    {
        $params = ['IPAddress' => $ip];

        $client = new SoapClient('http://www.webservicex.net/geoipservice.asmx?WSDL');
        $result = $client->GetGeoIP($params);

        if (!is_object($result) || empty($result->GetGeoIPResult)) {
            $result = null;
        } else {
            $result = $result->GetGeoIPResult;
        }

        return $result;
    }

    public function getDataByIpCached($ip)
    {
        $cache = new CPHPCache();
        $cacheTime = 60 * 60 * 24 * 7;
        $cacheId = __METHOD__ . '.' . $ip;
        $cachePath = __CLASS__;

        if ($cacheTime > 0 && $cache->InitCache($cacheTime, $cacheId, $cachePath)) {

            $res = $cache->GetVars();
            $result = $res['result'];

        } else {

            $result = $this->getDataByIpRaw($ip);

            $cache->StartDataCache($cacheTime, $cacheId, $cachePath);
            $cache->EndDataCache(['result' => $result]);

        }

        return $result;
    }

    public function addError2Log($error, $data)
    {
        $log = new CEventLog();
        $log->Log('WARNING', __CLASS__, 'app', $data, $error, SITE_ID);
    }
}