<?php
if ($arResult['ERROR']) {
    echo $arResult['ERROR'];
} else {
    $result = [];

    ob_start();
    require dirname(__DIR__) . '/.default/result.php';
    $result['html'] = ob_get_clean();

    echo json_encode($result);
}