<p>GeoIP поиск. По заданному IP-адресу определяется страна пользователя.</p>

<form class="form-geoip js-form-geoip1" method="post" action="">
    <input type="hidden" name="form" value="geoip">
    <?= bitrix_sessid_post() ?>

    <p>
        <input class="form-geoip__input-text" type="text" name="ip" value="<?= $arParams['IP'] ?>"
               placeholder="IP-адрес">
        <input class="form-geoip__submit" type="submit" value="Вычислить по IP">
    </p>
</form>

<div class="js-result">
    <? require 'result.php' ?>
</div>
