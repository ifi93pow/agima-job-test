$('.js-form-geoip').each(function () {
    var $form = $(this),
        $result = $form.siblings('.js-result');

    $form.on('submit', function (ev) {
        ev.preventDefault();

        $.ajax({
            url: $form.prop('action'),
            type: $form.prop('method') || 'post',
            dataType: 'json',
            data: $form.serialize(),
            success: function (result) {
                $result.html(result['html']);
            },
            error: function (xhr, resp, text) {
                $result.html('<div class="error-text">' + xhr.responseText + '</div>');
            }
        });

        return false;
    });
});
