<?php
if ($arResult['ERROR']) {
    echo $arResult['ERROR'];
} else {
    $result = [];

    ob_start();
    require dirname(__DIR__) . '/.default/items.php';
    $result['html'] = ob_get_clean();

    $result['nav'] = $arResult['NAV_STRING'];

    echo json_encode($result);
}