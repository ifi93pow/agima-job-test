<? foreach ($arResult['ITEMS'] as $item) { ?>
    <li>
        <ol>
            <li>Название: <?= $item['NAME'] ?></li>
            <li>Привязанные пользователи:
                <? if ($item['USERS']['VALUE']) { ?>
                    <ul>
                        <? foreach ($item['USERS']['VALUE'] as $subItemId) { ?>
                            <? if ($subItem = $arResult['USERS'][$subItemId]) { ?>
                                <li>
                                    <?= $subItem['NAME'] ?> <?= $subItem['LAST_NAME'] ?>
                                </li>
                            <? } ?>
                        <? } ?>
                    </ul>
                <? } else { ?>
                    отсутствуют
                <? } ?>
            </li>
            <li>Привязанные элементы:
                <? if ($item['ITEMS']['VALUE']) { ?>
                    <ul>
                        <? foreach ($item['ITEMS']['VALUE'] as $subItemId) { ?>
                            <? if ($subItem = $arResult['RELATED_ITEMS'][$subItemId]) { ?>
                                <li>
                                    <?= $subItem['ID'] ?>
                                </li>
                            <? } ?>
                        <? } ?>
                    </ul>
                <? } else { ?>
                    отсутствуют
                <? } ?>
            </li>
            <li>Дата изменения: <?= FormatDate('d.m.y H:i', MakeTimeStamp($item['TIMESTAMP_X'])) ?></li>
        </ol>
    </li>
<? } ?>