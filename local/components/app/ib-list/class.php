<?php

use Bitrix\Main\Entity\Query;
use App\Registry;

/**
 * @property \Bitrix\Main\Context $context
 * @property \Bitrix\Main\Server $server
 */
class CAppIbList extends CBitrixComponent
{
    public function __construct($component)
    {
        parent::__construct($component);

        \Bitrix\Main\Loader::includeModule('app');
        \Bitrix\Main\Loader::includeModule('iblock');

        $this->context = \Bitrix\Main\Application::getInstance()->getContext();
        $this->server = $this->context->getServer();
    }

    public function onPrepareComponentParams($arParams)
    {
        $arParams['IS_AJAX'] = $this->request->isAjaxRequest();

        $arParams['PAGE_NUM'] = (int)$this->request->get('page');
        if ($arParams['PAGE_NUM'] < 1) $arParams['PAGE_NUM'] = 1;
        $arParams['SEF_DIR'] = '/2-list/';

        return $arParams;
    }

    public function executeComponent()
    {
        global $APPLICATION;

        if ($this->arParams['IS_AJAX']) {
            $APPLICATION->RestartBuffer();
            $this->setTemplateName('ajax');
            header('Content-Type: application/json');
        }

        if ($this->startResultCache($this->arParams['CACHE_TIME'])) {
            $this->calcItems();
            $this->calcUsers();
            $this->calcRelatedItems();

            $this->setResultCacheKeys([]);
            $this->includeComponentTemplate();
        }

        if ($this->arParams['IS_AJAX']) {
            die;
        }
    }

    protected function calcItems()
    {
        $arOrder = [
            'ID' => 'DESC'
        ];
        $arFilter = [
            'IBLOCK_ID' => Registry::getIblockIdByCode($this->arParams['IBLOCK_CODE_1']),
            'ACTIVE' => 'Y'
        ];
        $arSelect = [
            'ID',
            'IBLOCK_ID',
            'NAME',
            'TIMESTAMP_X'
        ];
        $arNavStartParams = [
            'nPageSize' => $this->arParams['NEWS_COUNT'],
            'iNumPage' => $this->arParams['PAGE_NUM']
        ];

        $result = [];

        $rs = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
        while ($ob = $rs->GetNextElement(true, false)) {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();
            foreach ($arProps as $arProp) {
                $arFields[$arProp['CODE']] = $arProp;
            }
            $result[] = $arFields;
        }

        $this->arResult['NAV_STRING'] = $rs->GetPageNavStringEx(
            $navComponentObject,
            '',
            $this->arParams['PAGER_TEMPLATE'],
            false,
            $this,
            array('SEF_DIR' => $this->arParams['SEF_DIR'])
        );

        $this->arResult['ITEMS'] = $result;
    }

    protected function calcUsers()
    {
        $itemIds = $this->getPropsIds($this->arResult['ITEMS'], 'USERS');
        if (!$itemIds) return;

        $query = new Query(\Bitrix\Main\UserTable::getEntity());
        $query
            ->setFilter(['ID' => $itemIds])
            ->setSelect(['ID', 'NAME', 'LAST_NAME']);
        $result = $query->exec()->fetchAll(new \Bitrix\Main\Text\HtmlConverter());
        $this->arResult['USERS'] = $this->getItemsById($result);
    }

    protected function calcRelatedItems()
    {
        $itemIds = $this->getPropsIds($this->arResult['ITEMS'], 'ITEMS');
        if (!$itemIds) return;

        $query = new Query(\Bitrix\Iblock\ElementTable::getEntity());
        $query
            ->setFilter(['ID' => $itemIds, 'ACTIVE' => 'Y'])
            ->setSelect(['ID']);
        $result = $query->exec()->fetchAll(new \Bitrix\Main\Text\HtmlConverter());

        $this->arResult['RELATED_ITEMS'] = $this->getItemsById($result);
    }

    protected function getPropsIds(&$items, $propName)
    {
        $itemIds = [];
        foreach ($items as $item) {
            if (!$item[$propName]['VALUE']) continue;
            if (is_array($item[$propName]['VALUE'])) {
                $itemIds = array_merge($itemIds, $item[$propName]['VALUE']);
            } else {
                $itemIds[] = $item[$propName]['VALUE'];
            }
        }
        $itemIds = array_unique($itemIds);
        return $itemIds;
    }

    protected function getItemsById(&$items)
    {
        $result = [];
        foreach ($items as $i => $item) {
            $result[$item['ID']] = &$items[$i];
        }
        return $result;
    }
}