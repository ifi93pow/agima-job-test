<?php

$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(dirname(dirname(__DIR__))));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!\Bitrix\Main\Loader::includeModule('app')) {
    Bitrix\Main\ModuleManager::registerModule('app');
    LocalRedirect($APPLICATION->GetCurUri());
}

$migrationsDir = dirname(__DIR__) . '/migrations/';
$migrationsFiles = glob($migrationsDir . 'migration*.php');

echo "<pre>";

foreach ($migrationsFiles as $migrationFile) {
    try {
        echo "[RUN] $migrationFile\n";
        \App\Migrations\Migrations::instance()->run($migrationFile);
        echo "[SUCCESS]\n";
    } catch (Exception $e) {
        echo "[ERROR] " . $e->getMessage() . "\n" . $e->getTraceAsString() . "\n\n";
    }
}