<?php

namespace App;

class Registry
{
    static function getIblockIdByCode($iblockCode, $bRenew = false)
    {
        static $result = array();
        if (!isset($result[$iblockCode]) || $bRenew) {
            $arResult = \Bitrix\Iblock\IblockTable::getList([
                'select' => ['ID', 'CODE']
            ])->fetchAll();
            foreach ($arResult as $arItem) {
                $result[$arItem['CODE']] = $arItem['ID'];
            }
        }
        return $result[$iblockCode];
    }
}