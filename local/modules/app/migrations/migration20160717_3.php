<?php

class Migration20160717_3 extends \App\Migrations\AbstractMigration
{
    function run()
    {
        foreach ($this->dataGenerator->arFiosBoy as $i => $fio) {
            if (!isset($this->dataGenerator->arNamesBoy[$i])) continue;

            $this->createUser([
                'LOGIN' => "test$i",
                'NAME' => $this->dataGenerator->arNamesBoy[$i],
                'LAST_NAME' => $fio,
                'EMAIL' => "test$i@example.com",
                'PASSWORD' => "testp$i",
            ]);
        }
    }
}