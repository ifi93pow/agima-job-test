<?php

class Migration20160717_2 extends \App\Migrations\AbstractMigration
{
    function run()
    {
        $this->createIblockCode('ib-list2', [
            'IBLOCK_TYPE_ID' => 'ib-list',
            'NAME' => 'Привязанные элементы'
        ]);

        foreach ($this->dataGenerator->arNouns as $i => $sName) {
            $this->createIblockElement('ib-list2', [
                'NAME' => "{$this->dataGenerator->arAdjective[$i]} $sName"
            ]);
        }
    }
}