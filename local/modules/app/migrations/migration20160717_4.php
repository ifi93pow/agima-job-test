<?php

class Migration20160717_4 extends \App\Migrations\AbstractMigration
{
    function run()
    {
        $this->createIblockCode('ib-list', [
            'IBLOCK_TYPE_ID' => 'ib-list',
            'NAME' => 'Элементы'
        ]);

        $this->createIblockProperty('ib-list', [
            'NAME' => 'Привязанные пользователи',
            'CODE' => 'USERS',
            'MULTIPLE' => 'Y',
            'PROPERTY_TYPE' => 'S',
            'USER_TYPE' => 'UserID'
        ]);

        $this->createIblockProperty('ib-list', [
            'NAME' => 'Привязанные элементы',
            'CODE' => 'ITEMS',
            'MULTIPLE' => 'Y',
            'PROPERTY_TYPE' => 'E',
            'USER_TYPE' => 'EList',
            'LINK_IBLOCK_ID' => App\Registry::getIblockIdByCode('ib-list2')
        ]);

        
        for ($i = 0; $i < 100; $i++) {
            $this->createIblockElement('ib-list', [
                'NAME' => "Элемент $i "
                    . reset($this->dataGenerator->getRandItems($this->dataGenerator->arAdjective, 1))
                    . ' '
                    . reset($this->dataGenerator->getRandItems($this->dataGenerator->arNouns, 1)),
                'PROPERTY_VALUES' => [
                    'USERS' => $this->dataGenerator->getRandItems($this->dataGenerator->getAllUsersId(), 5),
                    'ITEMS' => $this->dataGenerator->getRandItems($this->dataGenerator->getAllElementsId('ib-list2'), 5),
                ]
            ]);
        }
    }
}