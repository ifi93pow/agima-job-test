(function () {
    var $infinitePaging;
    var $infinitePagingA;
    init();

    function init() {
        $infinitePaging = $('.js-infinite-paging');
        $infinitePagingA = $('.js-infinite-paging__a');
    }

    $(document).on('scroll', function () {
        $.each($infinitePaging, function () {
            if (isScrolledIntoView(this)) {
                loadItems();
            }
        });
    });

    $(document).on('click', '.js-infinite-paging__a', function (ev) {
        ev.preventDefault();
        loadItems();
        return false;
    });

    function isScrolledIntoView(el) {
        var elemTop = el.getBoundingClientRect().top;
        var elemBottom = el.getBoundingClientRect().bottom;

        var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
        return isVisible;
    }

    function removeNav() {
        $('.js-nav').empty();
    }

    function loadItems() {
        removeNav();
        $.ajax({
            url: $infinitePagingA.prop('href'),
            type: 'get',
            dataType: 'json',
            success: function (result) {
                $('.js-result').append(result['html']);
                $('.js-nav').html(result['nav']);
                init();
            },
            error: function (xhr, resp, text) {
                alert(xhr.responseText);
            }
        });
    }
}());