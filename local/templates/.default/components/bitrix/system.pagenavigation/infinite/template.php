<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? if ($arResult['NavPageNomer'] < $arResult['NavPageCount']) { ?>
    <div class="infinite-paging js-infinite-paging">
        <a class="js-infinite-paging__a" href="<?= $arParams['SEF_DIR']
            ? $arParams['SEF_DIR'] . ($arResult['NavPageNomer'] + 1) . '/'
            : $arResult["sUrlPath"] . '?' . $strNavQueryString . 'PAGEN_' . $arResult["NavNum"] . '=' . ($arResult["NavPageNomer"] + 1)
        ?>">Следующая страница</a>
    </div>
<? } ?>